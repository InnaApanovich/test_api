BASE_URL = 'https://restful-booker.herokuapp.com'

AUTH_CREATE_TOKEN = BASE_URL + '/auth'

BOOKING = BASE_URL + '/booking'

PING = BASE_URL + '/ping'

