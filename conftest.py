import pytest
from json import dumps, loads
import requests
from test_api import api_urls
from test_api.headers import HEADERS


@pytest.fixture()
def user_data():
    data = {
        "username" : "admin",
        "password" : "password123"
    }
    return data

@pytest.fixture()
def booking_data():
    book_data = {
        "firstname" : "Katie",
        "lastname" : "Mallow",
        "totalprice" : 110,
        "depositpaid" : True,
        "bookingdates" : {
            "checkin" : "2021-06-01",
            "checkout" : "2021-07-01"
        },
        "additionalneeds" : "Breakfast"
    }
    return book_data

@pytest.fixture()
def booking_token(user_data):
    auth_response = requests.post(
        url=api_urls.AUTH_CREATE_TOKEN,
        headers=HEADERS,
        json=user_data
    )
    booking_token = auth_response.json()["token"]
    assert auth_response.status_code == 200
    headers_token = {"Content-Type": "application/json",
                     "Cookie": f"token={booking_token}"}
    return headers_token

@pytest.fixture()
def update_booking_data():
    new_book_data = {
        "firstname" : "Katherine",
        "lastname" : "Mallow",
        "totalprice" : 110,
        "depositpaid" : True,
        "bookingdates" : {
            "checkin" : "2021-06-01",
            "checkout" : "2021-07-01"
        },
        "additionalneeds" : "Breakfast"
    }
    return new_book_data


@pytest.fixture()
def patch_booking_data():
    patched_data = {
        "totalprice" : 130,
        "additionalneeds" : "Breakfast and dinner"
    }
    return patched_data

@pytest.fixture()
def booking_id():
    get_booking_ids_response = requests.get(
        url=api_urls.BOOKING,
        headers=HEADERS
    )
    unique_booking_ids = loads(get_booking_ids_response.text)
    assert get_booking_ids_response.status_code == 200

    booking_id = str(unique_booking_ids[0]['bookingid'])
    return booking_id
