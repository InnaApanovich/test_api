import requests
from json import loads, dumps
from test_api import api_urls
from test_api.headers import HEADERS

class TestAPI:

    def test_health_check(self):
        health_check_response = requests.get(
            url=api_urls.PING
        )
        assert health_check_response.status_code == 201

    def test_auth(self, user_data):
        auth_response = requests.post(
            url=api_urls.AUTH_CREATE_TOKEN,
            headers=HEADERS,
            data=dumps(user_data)
        )
        assert auth_response.status_code == 200

    def test_get_booking_ids(self):
        get_booking_ids_response = requests.get(
            url=api_urls.BOOKING,
            headers=HEADERS
        )
        assert get_booking_ids_response.status_code == 200

    def test_get_booking_by_id(self, booking_id):
        get_booking_response = requests.get(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=HEADERS
        )
        assert get_booking_response.status_code == 200

    def test_create_booking(self, booking_data):
        create_booking_response = requests.post(
            url=api_urls.BOOKING,
            headers=HEADERS,
            data=dumps(booking_data)
        )
        assert create_booking_response.status_code == 200

    def test_update_booking(self, update_booking_data, booking_token, booking_id):
        update_booking_response = requests.put(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=booking_token,
            data=dumps(update_booking_data),
        )
        assert update_booking_response.status_code == 200
        get_booking_response = requests.get(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=HEADERS
        )
        assert loads(get_booking_response.text) == update_booking_data

    def test_patch_booking(self, patch_booking_data, booking_token, booking_id):
        partial_update_booking_response = requests.patch(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=booking_token,
            data=dumps(patch_booking_data),
        )
        assert partial_update_booking_response.status_code == 200
        get_booking_response = requests.get(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=HEADERS
        )
        changed_data = loads(get_booking_response.text)
        assert all(item in changed_data for item in patch_booking_data) is True
        assert [get_booking_response.json()["additionalneeds"], get_booking_response.json()["totalprice"]] == \
               [patch_booking_data["additionalneeds"], patch_booking_data["totalprice"]]


    def test_delete_booking(self, booking_token, booking_id):
        delete_booking_response = requests.delete(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=booking_token,
        )
        assert delete_booking_response.status_code == 201
        get_booking_response = requests.get(
            url=api_urls.BOOKING + f'/{booking_id}',
            headers=HEADERS
        )
        assert get_booking_response.status_code == 404

